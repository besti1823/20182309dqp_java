package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.io.*;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AutoCompleteTextView put1 = findViewById(R.id.text1);
        AutoCompleteTextView put2 = findViewById(R.id.text2);
        AutoCompleteTextView put3 = findViewById(R.id.text3);
        AutoCompleteTextView put4 = findViewById(R.id.text4);
        AutoCompleteTextView put5 = findViewById(R.id.text5);
        AutoCompleteTextView put6 = findViewById(R.id.text6);
        AutoCompleteTextView put7 = findViewById(R.id.text7);
        AutoCompleteTextView put8 = findViewById(R.id.text8);

        int[] a = new int[12];
        int[] b = new int[12];
        int[] c = new int[12];

        a[0] = 2;
        a[1] = 20;
        a[2] = 17;
        a[3] = 27;
        a[4] = 14;
        a[5] = 4;
        a[6] = 8;
        a[7] = 5;
        a[8] = 18;
        a[9] = 9;
        a[10] = 23;
        a[11] = 7;

        b[0] = 2;
        b[1] = 74;
        b[2] = 29;
        b[3] = 27;
        b[4] = 11;
        b[5] = 90;
        b[6] = 24;
        b[7] = 6;
        b[8] = 48;
        b[9] = 9;
        b[10] = 22;
        b[11] = 3;

        c[0] = 2;
        c[1] = 57;
        c[2] = 39;
        c[3] = 27;
        c[4] = 15;
        c[5] = 68;
        c[6] = 35;
        c[7] = 5;
        c[8] = 23;
        c[9] = 9;
        c[10] = 73;
        c[11] = 61;


        String text1 = null;
        String text2 = null;
        String text3 = null;
        String text4 = null;
        String text5 = null;
        String text6 = null;

        for(int i = 0; i < 12; i++)
            text1 += (a[i]+" ");
        put1.setText(text1);

        Sorting.shellSort(a);//???????

        for(int i = 0; i < 12; i++)
            text2 += (a[i]+" ");
        put2.setText(text2);

        for(int i = 0; i < 12; i++)
            text3 += (b[i]+" ");
        put3.setText(text3);

        Sorting.heapSort(b);//??????

        for(int i = 0; i < 12; i++)
            text4 += (b[i]+" ");
        put4.setText(text4);

        for(int i = 0; i < 12; i++)
            text5 += (c[i]+" ");
        put5.setText(text5);
        Sorting.Node cNode = new Sorting.Node();//??????????
        for(int i = 0;i < 12; i++)
            cNode.add(c[i]);
        put6.setText(cNode.values().toString());

        put7.setText(Searching.insertionSearch(a,18,0,11));
        put8.setText(Searching.fibonacciSearch(a,12,23));
    }
}


