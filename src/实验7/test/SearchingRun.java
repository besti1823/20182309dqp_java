package test;

import cn.edu.besti.cs1823.D2309.*;

public class SearchingRun {
    public static void main(String[] args) {
        Comparable[] a = new Comparable[12];

        a[0] = 19;
        a[1] = 14;
        a[2] = 23;
        a[3] = 9;
        a[4] = 68;
        a[5] = 20;
        a[6] = 84;
        a[7] = 27;
        a[8] = 55;
        a[9] = 11;
        a[10] = 10;
        a[11] = 79;

        System.out.print("排序前：");
        for(int i = 0; i < 12; i++) System.out.print(a[i]+" ");
        System.out.println();

        Sorting.SelectionSort(a);

        System.out.print("排序后：");
        for(int i = 0; i < 12; i++) System.out.print(a[i]+" ");
        System.out.println();

        System.out.print("倒  序：");
        for(int i = 11; i >= 0; i--) System.out.print(a[i]+" ");
        System.out.println();

        System.out.println("(正常)找到："+Searching.linearSearch(a, 20));
        System.out.println("(异常)找到："+Searching.linearSearch(a, 4));
        System.out.println("(边界)找到："+Searching.linearSearch(a, 9));
    }
}
