import java.io.*;

public class FileTest3 {
    public static void main(String[] args) throws IOException {
        File file = new File("Test3.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "BufferedOutputStream";
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!Writer");
        writer2.flush();

        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
        }
    }
}
