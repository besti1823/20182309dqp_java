import java.io.*;

public class FileTest4 {
    public static void main(String[] args) throws IOException {
        File file = new File("Test4.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test2 = {'T','e','s','t','4','!'};
        byte[] buffer = new byte[1024];
        outputStream1.write(test2);
        String content = "";

        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content =bufferedReader.readLine())!= null){
            System.out.println(content);
        }
    }
}
