import java.io.*;

public class ComplexIOC {
    public static void main(String[] args) throws IOException{
        File file = new File("ComplexTXT.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        //д
        OutputStream outputStream1 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream1 = new BufferedOutputStream(outputStream1);
        String content1 = "2+3i+4+5i";
        bufferedOutputStream1.write(content1.getBytes(),0,content1.getBytes().length);
        bufferedOutputStream1.flush();
        bufferedOutputStream1.close();
        //��
        byte[] buffer = new byte[1024];
        String info = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            info += new String(buffer,0,flag);
        }

        int i,j,k;

        for(i=0,j=0;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double a = Double.parseDouble(info.substring(i,j));

        for(i=j++;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double b = Double.parseDouble(info.substring(i,j));
        if(info.charAt(i-1)=='-')b=-b;

        //�����
        j++;
        k=j;

        for(i=j++;j<info.length();j++){
            if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
        }
        double c = Double.parseDouble(info.substring(i,j));

        i=j+1;
        j=info.length()-1;
        double d = Double.parseDouble(info.substring(i,j));
        if(info.charAt(i-1)=='-')d=-d;

        Complex x = new Complex(a,b);
        Complex y = new Complex(c,d);

        String answer = (x.complexAdd(y)).toString();

        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "2+3i+4+5i="+answer;
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

        Reader reader2 = new FileReader(file);
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
        }
    }
}
