
//***********************************************
//
//     Filename: chengxu2_6.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-15 16:13:43
//Last Modified: 2019-09-15 16:13:43
//***********************************************

public class chengxu2_6
{
        public static void main(String[] args)
        {
                int sides = 7;  // declaration with initialization

		System.out.println( "A heptagon has " + sides + " sides.");

		sides = 10;  // assignment statement

		System.out.println ("A decagon has " + sides + " sides. ");

		sides = 12;

		System.out.println ("A dodecagon has " + sides + " sides.");
        }
}
