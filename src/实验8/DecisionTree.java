import java.util.Scanner;

public class DecisionTree {
    public static void main(String[] args) throws EmptyCollectionException {
        LinkedBinaryTree<String> ques12 = new LinkedBinaryTree<String>("Eat dumplings!");
        LinkedBinaryTree<String> ques13 = new LinkedBinaryTree<String>("Eat noodles!");

        LinkedBinaryTree<String> ques9 = new LinkedBinaryTree<String>("Eat vegetable!");
        LinkedBinaryTree<String> ques8 = new LinkedBinaryTree<String>("Eat meet!");
        LinkedBinaryTree<String> ques10 = new LinkedBinaryTree<String>("Dumplings?", ques12, ques13);
        LinkedBinaryTree<String> ques11 = new LinkedBinaryTree<String>("GET OUT!!!");

        LinkedBinaryTree<String> ques1 = new LinkedBinaryTree<String>("Eat bread!");
        LinkedBinaryTree<String> ques2 = new LinkedBinaryTree<String>("Eat egg!");
        LinkedBinaryTree<String> ques6 = new LinkedBinaryTree<String>("Dinner?", ques10, ques11);
        LinkedBinaryTree<String> ques7 = new LinkedBinaryTree<String>("Meet?", ques8, ques9);

        LinkedBinaryTree<String> ques3 = new LinkedBinaryTree<String>("Bread?", ques1, ques2);
        LinkedBinaryTree<String> ques5 = new LinkedBinaryTree<String>("Lunch?", ques7, ques6);

        LinkedBinaryTree<String> ques4 = new LinkedBinaryTree<String>("Breakfast?", ques3, ques5);

        LinkedBinaryTree<String> quesPrint = ques4;
        Scanner scan = new Scanner(System.in);
        String rpy;
//        boolean brk = false;

//        String able1 = "Y";
//        String able2 = "y";
//        String able3 = "N";
//        String able4 = "n";

        for(;;){
            System.out.println(quesPrint.getRootElement());
            if(quesPrint.root.left == null && quesPrint.root.right == null)
                break;

            rpy = scan.nextLine();
            for (;;){
                if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 || rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                    break;
                else
                    System.out.println("Entered Error! Please enter again");
            }
            if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 )
                    quesPrint = quesPrint.getLeft();
            if(rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                    quesPrint = quesPrint.getRight();
        }
        System.out.println("Have a nice day!!");
    }
}