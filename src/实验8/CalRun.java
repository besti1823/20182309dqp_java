public class CalRun {
    public static void main(String[] args) {
        String exp = "5+2*(3*(2-1))";
        System.out.println(ExpressionTransform.In2Post(exp));
        System.out.println(ExpressionTransform.CalculatePost(ExpressionTransform.In2Post(exp)));
    }
}
