public class GrowTree {
    public static BTNode<Character> create(Character[] post, int postLeft, int postRight, Character[] in, int inLeft, int inRight){
        if(postLeft > postRight)
            return null;
        BTNode<Character> root = new BTNode<Character>(post[postRight]);//get root

        //find root from infix
        int rootIndex = -1;
        for(int i = inLeft; i <= inRight; i++){
            if(in[i] == root.getElement()){
                rootIndex = i;
                break;
            }
        }

        //find number of left tree
        int leftNumber = rootIndex - inLeft;

        //recursion
        root.setLeft(create(post, postLeft, postLeft+leftNumber-1, in, inLeft, rootIndex-1));
        root.setRight(create(post, postLeft+leftNumber, postRight-1, in, rootIndex+1, inRight));

        return root;
    }




//    public static TreeNode create(int[] post, int postLeft,
//                                  int postRight,
//                                  int[] in, int inLeft, int inRight) {
//        if (postLeft > postRight) {
//            return null;
//        }
//        TreeNode root = new TreeNode(post[postRight]);
//
//        // 从中序遍历中找出根节点
//        int findIndex = -1;
//        for (int i=inLeft; i<=inRight; ++i) {
//            if (in[i]==root.data) {
//                findIndex = i;
//                break;
//            }
//        }
//
//        // 计算左子树的数量
//        int leftNum = findIndex - inLeft;
//        root.left = create(post, postLeft, postLeft+leftNum-1,
//                in, inLeft, findIndex-1);
//
//        root.right = create(post, postLeft+leftNum, postRight-1,
//                in, findIndex+1, inRight);
//
//        return root;
//    }

}
