public class ElementNotFoundException extends Throwable {
    public ElementNotFoundException(String s) {
        super(s);
    }
}
