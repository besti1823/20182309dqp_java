public class EmptyCollectionException extends Throwable {
    public EmptyCollectionException(String s) {
        super(s);
    }
}
