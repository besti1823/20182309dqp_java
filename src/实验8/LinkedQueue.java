public class LinkedQueue<T>
{
    private int count;
    private LinearNode<T> front, rear;
    //-----------------------------------------------------------------
// Creates an empty queue.
//-----------------------------------------------------------------
    public LinkedQueue()
    {
        count = 0;
        front = rear = null;
    }
    //-----------------------------------------------------------------
// Adds the specified element to the rear of this queue.
//-----------------------------------------------------------------
    public void enqueue (T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);
        if (count == 0)
            front = node;
        else
            rear.setNext(node);
        rear = node;
        count++;
    }

    public T dequeue () throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException ("Get queue operation "
                    + "failed. The queue is empty.");
        else {
            count--;
            LinearNode<T> temp = front;
            front = front.getNext();
            return temp.getElement();
        }
    }

    public boolean isEmpty() {
        return (front == rear);
    }

    public T first () throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException ("Get queue operation "
                    + "failed. The queue is empty.");
        else
            return front.getElement();
    }

    public int size() {
        return count;
    }

    public String toString() {
        String rpy = null;
        LinearNode<T> temp = front;
        for( ; temp != null; temp = temp.getNext())
            rpy += temp.getElement();
        return rpy;
    }
}