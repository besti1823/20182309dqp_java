package TrueWorkSpace;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class HuffmanTree {
    public static HuffmanNode createTree(List<TrueWorkSpace.HuffmanNode> nodes) {
        while (nodes.size() > 1) {
            Collections.sort(nodes);

            HuffmanNode left = nodes.get(nodes.size() - 2);
            left.setCode("0");
            HuffmanNode right = nodes.get(nodes.size() - 1);
            right.setCode("1");
            HuffmanNode parent = new HuffmanNode(null, left.getCount() + right.getCount());
            parent.setLeftChild(left);
            parent.setRightChild(right);
            nodes.remove(left);
            nodes.remove(right);
            nodes.add(parent);
        }
        return nodes.get(0);
    }

    public List<HuffmanNode> breadth(HuffmanNode root) {
        List<HuffmanNode> list = new ArrayList<HuffmanNode>();
        Queue<HuffmanNode> queue = new ArrayDeque<HuffmanNode>();

        if (root != null) {
            queue.offer(root);
            root.getLeftChild().setCode(root.getCode() + "0");
            root.getRightChild().setCode(root.getCode() + "1");
        }

        while (!queue.isEmpty()) {
            list.add(queue.peek());
            HuffmanNode node = queue.poll();
            if (node.getLeftChild() != null)
                node.getLeftChild().setCode(node.getCode() + "0");
            if (node.getRightChild() != null)
                node.getRightChild().setCode(node.getCode() + "1");

            if (node.getLeftChild() != null) {
                queue.offer(node.getLeftChild());
            }

            if (node.getRightChild() != null) {
                queue.offer(node.getRightChild());
            }
        }
        return list;
    }
}
