package TrueWorkSpace;

public class HuffmanNode implements Comparable<HuffmanNode>{
    private String name;
    private String code;
    private double count;
    private HuffmanNode leftChild;
    private HuffmanNode rightChild;

    public HuffmanNode(String name,double count, HuffmanNode leftChild, HuffmanNode rightChild) {
        this.name = name;
        this.count = count;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HuffmanNode(String name, double count) {
        this.name = name;
        this.count = count;
        this.code = "";
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public HuffmanNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(HuffmanNode leftChild) {
        this.leftChild = leftChild;
    }

    public HuffmanNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(HuffmanNode rightChild) {
        this.rightChild = rightChild;
    }

    public String toString() {
        return "name: " + name + " count: " + count + " 编码为: " + code +"\n";
    }

    public int compareTo(HuffmanNode node) {
        if (this.count >= node.count){
            return 1;
        }
        else {
            return -1;
        }
    }
}
