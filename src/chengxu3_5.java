
//***********************************************
//
//     Filename: chengxu3_5.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-18 13:37:23
//Last Modified: 2019-09-18 13:37:23
//***********************************************
import java.util.Scanner;
import java.text.DecimalFormat;
public class chengxu3_5
{
        public static void main(String[] args)
        {
        	int radius;
		double area, circumference ;
		Scanner scan = new Scanner (System.in);

		System.out.print ("Enter the circle's radius: ");
		radius = scan.nextInt();
		area = Math.PI * Math.pow(radius, 2);
		circumference = 2 * Math.PI * radius;
		// Round the output to three decimal places
		DecimalFormat fmt = new DecimalFormat ( "0.###");

		System.out.println ("The circle's area:"+ fmt.format (area)) ;
		System.out.println ("The circle's circumference: " + fmt.format (circumference));
        }
}
