package test;

import java.util.Scanner;

public class GraphTest {
    int[][] adjMatrix;
    int num1, num2;
    Scanner input = new Scanner(System.in);


    // 构建函数
    public void create() {
        System.out.println("请输入结点数:");
        num1 = input.nextInt();

        System.out.println("请输入边数:");
        num2 = input.nextInt();
        adjMatrix = new int[num1][num1];
        int i = 0;
        int vexA, vexB;
        while (i < num2) {
            System.out.println("请输入相连接的结点");
            vexA = input.nextInt();
            while (vexA < 1 || vexA > num1)
            {
                System.out.println("范围不符合，请重新输入：");
                vexA = input.nextInt();
            }
            vexB = input.nextInt();
            while (vexB < 1 || vexB > num1)
            {
                System.out.println("范围不符合，请重新输入：");
                vexB = input.nextInt();
            }
            adjMatrix[vexA - 1][vexB - 1] = 1;
            adjMatrix[vexB - 1][vexA - 1] = 1;

            if (i == num2 - 1)
            {
                System.out.println("Ending!");
            }
            else
            {
                System.out.println("Continue!");
            }
            i++;
        }
    }


    // 打印邻接矩阵
    public void play() {
        System.out.println("该邻接矩阵为：");
        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num1; j++) {
                System.out.print(adjMatrix[i][j]+"     ");
            }
            System.out.println();
            System.out.println();
        }
    }

    // 添加节点
    public void add(int n)//加几个结点
    {
        num1= num1+n;
    }
    //    // 删除节点
//    public void delete(int n)//输入要删除的结点名（第几个）
//    {
//        if (n < 1 || n > num1)
//        {
//            System.out.println("范围不符合，请重新输入：");
//            n = input.nextInt();
//        }
//        else
//        {
//
//        }
//    }
    // 添加边
    public void addB()
    {
        int vexA,vexB;
        System.out.println("请输入相连接的结点");
        vexA = input.nextInt();
        while (vexA < 1 || vexA > num1)
        {
            System.out.println("范围不符合，请重新输入：");
            vexA = input.nextInt();
        }
        vexB = input.nextInt();
        while (vexB < 1 || vexB > num1)
        {
            System.out.println("范围不符合，请重新输入：");
            vexB = input.nextInt();
        }
        adjMatrix[vexA - 1][vexB - 1] = 1;
        adjMatrix[vexB - 1][vexA - 1] = 1;
    }
    // 删除边
    public void deleteB()
    {
        int vexA,vexB;
        System.out.println("请输入相连接的结点");
        vexA = input.nextInt();
        while (vexA < 1 || vexA > num1)
        {
            System.out.println("范围不符合，请重新输入：");
            vexA = input.nextInt();
        }
        vexB = input.nextInt();
        while (vexB < 1 || vexB > num1)
        {
            System.out.println("范围不符合，请重新输入：");
            vexB = input.nextInt();
        }
        adjMatrix[vexA - 1][vexB - 1] = 0;
        adjMatrix[vexB - 1][vexA - 1] = 0;
    }
    // 结点数
    public int size()
    {
        return num1;
    }
    // 是否为空
    public boolean isEmpty()
    {
        if (num1 == 0)
            return true;
        else
            return false;
    }
    // 广度优先迭代器

    // 深度优先迭代器



}
