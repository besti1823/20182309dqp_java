
//***********************************************
//
//     Filename: pp2_4.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-15 17:59:33
//Last Modified: 2019-09-15 17:59:33
//***********************************************
import java.util.Scanner;
public class pp2_4
{
        public static void main(String[] args)
        {
                double f,c;
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter temp F:");
		f=scan.nextDouble();
		c=(f-32)/1.8;
		System.out.println("Temp C:"+c);
        }
}
