import java.util.Stack;

public class BinaryTree {
    private TreeNode root = null;
    private int size = 0;
    private TreeNode next = root;

    public int countSize() {
        return size;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void add(char element) {
        if(root == null)
            root.setValue(element);
    }

    public static void preOrderRe(TreeNode biTree)
    {//递归实现
        System.out.print(biTree.getValue());
        TreeNode leftTree = biTree.getLeftChild();
        if(leftTree != null)
        {
            preOrderRe(leftTree);
        }
        TreeNode rightTree = biTree.getRightChild();
        if(rightTree != null)
        {
            preOrderRe(rightTree);
        }
    }

    public static void preOrder(TreeNode biTree)
    {//非递归实现
        Stack<TreeNode> stack = new Stack<TreeNode>();
        while(biTree != null || !stack.isEmpty())
        {
            while(biTree != null)
            {
                System.out.print(biTree.getValue());
                stack.push(biTree);
                biTree = biTree.getLeftChild();
            }
            if(!stack.isEmpty())
            {
                biTree = stack.pop();
                biTree = biTree.getRightChild();
            }
        }
    }

    public static void midOrderRe(TreeNode biTree)
    {//中序遍历递归实现
        if(biTree == null)
            return;
        else
        {
            midOrderRe(biTree.getLeftChild());
            System.out.print(biTree.getValue());
            midOrderRe(biTree.getRightChild());
        }
    }

    public static void midOrder(TreeNode biTree)
    {//中序遍历费递归实现
        Stack<TreeNode> stack = new Stack<TreeNode>();
        while(biTree != null || !stack.isEmpty())
        {
            while(biTree != null)
            {
                stack.push(biTree);
                biTree = biTree.getLeftChild();
            }
            if(!stack.isEmpty())
            {
                biTree = stack.pop();
                System.out.print(biTree.getValue());
                biTree = biTree.getRightChild();
            }
        }
    }

    public static void postOrderRe(TreeNode biTree)
    {//后序遍历递归实现
        if(biTree == null)
            return;
        else
        {
            postOrderRe(biTree.getLeftChild());
            postOrderRe(biTree.getRightChild());
            System.out.print(biTree.getValue());
        }
    }
}

