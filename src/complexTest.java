import junit.framework.TestCase;
import org.junit.Test;
public class complexTest extends TestCase {
    complex a=new complex(2.0,5.0);
    complex b=new complex(3.0,-4.0);
    complex c=new complex(5.0,1.0);
    complex d=new complex(-1.0,9.0);
    complex e=new complex(26.0,7.0);
    complex f=new complex(-2.0,-3.0);
    
    public void testNormal(){
        assertEquals(c.toString(), (a.complexAdd(b)).toString());
        assertEquals(d.toString(), (a.complexSub(b)).toString());
        assertEquals(e.toString(), (a.complexMulti(b)).toString());
        assertEquals(f.toString(), (a.complexDiv(b)).toString());
    }
}
