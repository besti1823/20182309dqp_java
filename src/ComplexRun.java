import java.util.Scanner;

public class ComplexRun {
    public static void main(String[] args) {
        Complex a = new Complex(3,4);
        Complex b = new Complex(6,7);

        System.out.println(a.complexAdd(b));
        System.out.println(a.complexSub(b));
        System.out.println(a.complexMulti(b));
        System.out.println(a.complexDiv(b));
    }
}
