import java.util.Stack;

public class ArrayStack<T> extends Stack<T> {
    private int count = 0;
    private T[] stack;
    private final int DC=10;

    public ArrayStack(){
        count = 0;
        stack = (T[])(new Object[DC]);
    }

    public T peek()
    {
        if(count<0)return (T) "Nothing";
        else return stack[count-1];
    }

    public boolean isEmpty()
    {
        if(count==0)return true;
        else return false;
    }

    public int size()
    {
        return count;
    }

    public String toString()
    {
        String rpy = null;
        int i;

        for(i=0;i<count;i++){
            rpy+=stack[i];
        }
        return rpy;
    }
}
