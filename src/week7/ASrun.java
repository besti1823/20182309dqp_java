public class ASrun {
    public static void main(String[] args) {
        ArrayStack test = new ArrayStack();

        System.out.println(test.isEmpty());

        test.push("A");
        test.push("B");
        test.push("C");
        test.push("D");
        test.push("E");
        test.push("F");

        System.out.println(test.peek());
        System.out.println(test.isEmpty());
        System.out.println(test.size());
        System.out.println(test.toString());

    }
}
