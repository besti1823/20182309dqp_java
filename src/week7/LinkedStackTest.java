public class LinkedStackTest {
    public static void main(String[] args) {
        LinkedStack stack = new LinkedStack();
        System.out.println("Empty?:" + stack.isEmpty());
        stack.push(1);
        stack.push(2);
        stack.push(3);

        System.out.println("Number" + stack.size());
        System.out.println("TOP:" + stack.peek());
        stack.pop();
        System.out.println("OUT:" + stack.toString());
    }
}
