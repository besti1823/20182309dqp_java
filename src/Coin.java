
//***********************************************
//
//     Filename: Coin.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-25 21:02:56
//Last Modified: 2019-09-25 21:02:56
//***********************************************
public class Coin {
    private final int HEADS = 0; // tails is 1
    private int face; //current side showing

    //--- - -
    // Sets up this coin by flipping it initially.
    public Coin() {
        flip();
    }

    // Flips this coin by randomly choosing a face value.
    public void flip() {
        face = (int) (Math.random() * 2);
    }

    // Returns true if the current face of this coin is heads.
    //------- ----- -
    public boolean isHeads() {
        return (face == HEADS);
    }
    //---------
    //------- _----- ______
    //-. Returne the current face of this coin as a string.
    public String toString( )
    {
        return (face == HEADS) ? "Heads" : "Tails";
    }
}

