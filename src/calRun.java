import java.util.Scanner;
public class calRun {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("1.运算复数");
        System.out.println("2.运算有理数");
        System.out.println("0.退出");

        int r;
        r = scan.nextInt();

        if(r==0){
            System.out.println("EXIT");
        }

        if(r==1){
            String info;
            info = scan.next();

            int i,j,k;

            for(i=0,j=0;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
            }
            double a = Double.valueOf(info.substring(i,j));

            for(i=j++;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
            }
            double b = Double.valueOf(info.substring(i,j));
            if(info.charAt(i-1)=='-')b=-b;

            //运算符
            j++;
            k=j;

            for(i=j++;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57||info.charAt(j)==46))break;
            }
            double c = Double.valueOf(info.substring(i,j));

            i=j+1;
            j=info.length()-1;
            double d = Double.valueOf(info.substring(i,j));
            if(info.charAt(i-1)=='-')d=-d;

            complex x = new complex(a,b);
            complex y = new complex(c,d);
            complex answer = new complex(0,0);
            //计算并给客户一个响应
            switch ((int)info.charAt(k)){
                case 43:
                    answer = x.complexAdd(y);
                    break;
                case 45:
                    answer = x.complexSub(y);
                    break;
                case 42:
                    answer = x.complexMulti(y);
                    break;
                case 47:
                    answer = x.complexDiv(y);
                    break;
                default:
                    System.out.println("ERROR!");
            }
            System.out.println(answer.toString());
        }
        if(r==2){
            String info;
            info = scan.next();

            int i,j,k;

            for(i=0,j=0;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57))break;
            }
            int a = Integer.valueOf(info.substring(i,j));

            for(i=j++;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57))break;
            }
            int b = Integer.valueOf(info.substring(i,j));
            if(info.charAt(i-1)=='-')b=-b;

            //运算符
            k=j;

            for(i=j++;j<info.length();j++){
                if(!(info.charAt(j)>=48&&info.charAt(j)<=57))break;
            }
            int c = Integer.valueOf(info.substring(i,j));

            i=j+1;
            j=info.length();
            int d = Integer.valueOf(info.substring(i,j));
            if(info.charAt(i-1)=='-')d=-d;

            RealNumber x = new RealNumber(a,b);
            RealNumber y = new RealNumber(c,d);
            RealNumber answer = new RealNumber(0,0);
            //计算并给客户一个响应
            switch (Integer.valueOf(info.substring(k))){
                case 43:
                    answer = x.Add(y);
                    break;
                case 45:
                    answer = x.Sub(y);
                    break;
                case 42:
                    answer = x.Multi(y);
                    break;
                case 47:
                    answer = x.Div(y);
                    break;
                default:
                    System.out.println("ERROR!");
            }
            System.out.println(answer.toString());
        }
    }
}
