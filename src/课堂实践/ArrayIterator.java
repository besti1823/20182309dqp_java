package javafoundations;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterable<T> {
    private final int DEFAULT_CAPACITY = 20;

    private T[] ary;
    private int count;

    public ArrayIterator() {
        count = 0;
        ary = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public void expandCapacity(){
        T[] longer = (T[])(new Object[ary.length*2]);
        for(int index = 0; index < count; index++)
            longer[index] = ary[index];
        ary = longer;
    }

    public void add(T element){
        ary[count++] = element;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public String toString(){
        String rpy = "";

        for (int index = 0; index < count; index++)
            rpy += ary[index]+" ";
        return rpy;
    }
}
