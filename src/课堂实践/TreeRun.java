public class TreeRun {
    public static void main(String[] args) {
        char[] message = new char[]{'A','B','#','C','D','#','#','#','E','#','F','#','#'};

        BinarySortTree tr = new BinarySortTree();

        for (int i  = 0; i < 12; i++){
                tr.add(message[i]);
        }

        //层序遍历
        BinarySortTree.levelOrder(tr);
    }
}
