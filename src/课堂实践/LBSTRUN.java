package test;

import javafoundations.LinkedBinarySearchTree;
import javafoundations.exceptions.ElementNotFoundException;

public class LBSTRUN {
    public static void main(String[] args) throws ElementNotFoundException {
        LinkedBinarySearchTree<Integer> lbst = new LinkedBinarySearchTree<>();

        lbst.add(10);
        lbst.add(18);
        lbst.add(3);
        lbst.add(8);
        lbst.add(12);
        lbst.add(2);
        lbst.add(7);
        lbst.add(3);

        System.out.println(lbst.inorder());
        //���ҳɹ�
        System.out.println(lbst.find(8));
        //����ʧ��
        try {
            System.out.println(lbst.find(9));
        }
        catch (ElementNotFoundException s) {
            System.out.println(s);
        }

        lbst.add(1);
        lbst.add(99);
        //����
        System.out.println(lbst.inorder());

        lbst.remove(2);
        lbst.remove(3);
        lbst.remove(1);
        //ɾ��
        System.out.println(lbst.inorder());
    }

}
