import java.util.Scanner;

public class BB84Run {
    public static void main(String[] args) {
        //确定Alice使用的基
        Scanner scan = new Scanner(System.in);

        //输入密码长度
        System.out.print("请输入密码长度：");
        int codeLength = scan.nextInt();
        QuantumNode[] quantumNodes = new QuantumNode[codeLength];
        String[] aBitCode = new String[codeLength];

        int aBasedOn;
        for(int i = 0; i < codeLength; i++){
            aBasedOn = (int)(Math.random()*4);
            if(aBasedOn == 0)
                quantumNodes[i] = new QuantumNode("0");
            else if(aBasedOn == 1)
                quantumNodes[i] = new QuantumNode("1");
            else if(aBasedOn == 2)
                quantumNodes[i] = new QuantumNode("-");
            else
                quantumNodes[i] = new QuantumNode("+");
        }

        System.out.println("Alice发出的密码为：");
        for(int i = 0; i < codeLength; i++){
            System.out.print(quantumNodes[i].toString());
        }
        System.out.println();

        for(int i = 0; i < codeLength; i++){
            if(quantumNodes[i].getStatus().compareTo("0")==0 || quantumNodes[i].getStatus().compareTo("1")==0)
                aBitCode[i] = quantumNodes[i].getStatus();
            else
            if(quantumNodes[i].getStatus().compareTo("-")==0)
                aBitCode[i] = "0";
            else
                aBitCode[i] = "1";
        }
        System.out.println("二进制对应为：");
        for(int i = 0; i < codeLength; i++){
            System.out.print(aBitCode[i]);
        }
        System.out.println();

        //是否引入Eve
        System.out.print("是否有窃听Eve（0/1）：");
        int eveFlag;
        for(;;){
            eveFlag = scan.nextInt();
            if(eveFlag == 0 || eveFlag == 1)
                break;
            System.out.print("错误！重新输入（0/1）：");
        }

        QuantumNode[] eveMeasureReply = quantumNodes;
        if(eveFlag == 1){
            for(int i = 0; i < codeLength; i++){
                if((int)(Math.random()*2) == 0)
                    eveMeasureReply[i] = new QuantumNode(MeasureQuantum.zeroOne(quantumNodes[i]));
                else
                    eveMeasureReply[i] = new QuantumNode(MeasureQuantum.addSub(quantumNodes[i]));
            }
            System.out.println("Eve测量的结果为：");
            for(int i = 0; i < codeLength; i++){
                System.out.print(eveMeasureReply[i].toString());
            }
            System.out.println();

            String[] eBitCode = new String[codeLength];
            for(int i = 0; i < codeLength; i++){
                if(eveMeasureReply[i].getStatus().compareTo("0")==0 || eveMeasureReply[i].getStatus().compareTo("1")==0)
                    eBitCode[i] = eveMeasureReply[i].getStatus();
                else
                if(eveMeasureReply[i].getStatus().compareTo("-")==0)
                    eBitCode[i] = "0";
                else
                    eBitCode[i] = "1";
            }
            System.out.println("二进制对应为：");
            for(int i = 0; i < codeLength; i++){
                System.out.print(eBitCode[i]);
            }
            System.out.println();
        }


        //Bob测量密码
        QuantumNode[] bobMeasureReply = new QuantumNode[codeLength];

        for(int i = 0; i < codeLength; i++){
            if((int)(Math.random()*2) == 0)
                bobMeasureReply[i] = new QuantumNode(MeasureQuantum.zeroOne(eveMeasureReply[i]));
            else
                bobMeasureReply[i] = new QuantumNode(MeasureQuantum.addSub(eveMeasureReply[i]));
        }

        System.out.println("Bob测量的结果为：");
        for(int i = 0; i < codeLength; i++){
            System.out.print(bobMeasureReply[i].toString());
        }
        System.out.println();

        String[] bBitCode = new String[codeLength];
        for(int i = 0; i < codeLength; i++){
            if(bobMeasureReply[i].getStatus().compareTo("0")==0 || bobMeasureReply[i].getStatus().compareTo("1")==0)
                bBitCode[i] = bobMeasureReply[i].getStatus();
            else
                if(bobMeasureReply[i].getStatus().compareTo("-")==0)
                    bBitCode[i] = "0";
                else
                    bBitCode[i] = "1";
        }
        System.out.println("二进制对应为：");
        for(int i = 0; i < codeLength; i++){
            System.out.print(bBitCode[i]);
        }
        System.out.println();

        //统计准确率
        int count = 0;
        for(int i = 0; i < codeLength; i++){
            if(aBitCode[i].compareTo(bBitCode[i]) == 0)
                count++;
        }
        double matchRate = (double)count/codeLength;
        System.out.println("准确率为：" + matchRate);
    }
}
