public class MeasureQuantum {
    public static String zeroOne(QuantumNode a){
        if(a.getStatus().compareTo("0")==0 || a.getStatus().compareTo("1")==0)
            return a.getStatus();
        else {
            if((int)(Math.random()*2) == 0)
                return "0";
            else
                return "1";
        }
    }

    public static String addSub(QuantumNode a){
        if(a.toString().compareTo("-")==0||a.toString().compareTo("+")==0)
            return a.getStatus();
        else {
            if((int)(Math.random()*2) == 0)
                return "-";
            else
                return "+";
        }
    }
}
