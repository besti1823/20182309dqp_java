import javax.xml.stream.FactoryConfigurationError;

//***********************************************
//
//     Filename: Coin.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-25 21:02:56
//Last Modified: 2019-09-25 21:02:56
//***********************************************
public class Coin1 {
    private enum hb {HEAD,BACK}
    private hb a = hb.HEAD;
    private hb face =hb.HEAD;
    //--- - -
    // Sets up this coin by flipping it initially.
    public Coin1() {
        flip();
    }

    // Flips this coin by randomly choosing a face value.
    public void flip() {
        if(0 == (int) (Math.random() * 2)){
            a = hb.BACK;
        }
        else a =hb.HEAD;
    }

    // Returns true if the current face of this coin is heads.
    //------- ----- -
    public boolean isHeads() {
        return a.equals(face);
    }
    //---------
    //------- _----- ______
    //-. Returne the current face of this coin as a string.
    public String toString()
    {
        return (a.equals(face)) ? "Heads" : "Tails";
    }
}
