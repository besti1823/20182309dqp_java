import javax.security.auth.login.CredentialException;

public class CircularArrayQueueRun {
    public static void main(String[] args) {
        CircularArrayQueue caq = new CircularArrayQueue();

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());

        caq.enqueue(1);
        caq.enqueue(2);
        caq.enqueue(3);
        caq.enqueue(4);

        System.out.println(caq.toString());

        caq.enqueue(5);
        caq.enqueue(6);
        caq.enqueue(7);
        caq.enqueue(8);
        caq.enqueue(9);
        caq.enqueue(10);

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());

        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();

        System.out.println(caq.toString());

        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());
    }
}
