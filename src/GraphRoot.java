package MyHobby;

public class GraphRoot {
    int number;
    GraphEdge next;

    public GraphRoot(int num){
        number = num;
        next = null;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public GraphEdge getNext() {
        return next;
    }

    public void setNext(GraphEdge next) {
        this.next = next;
    }
}
