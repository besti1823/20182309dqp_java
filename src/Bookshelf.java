
//***********************************************
//
//     Filename: Bookshelf.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-20 19:41:47
//Last Modified: 2019-09-20 19:41:47
//***********************************************
import java.util.Scanner;
public class Bookshelf
{
        public static void main(String[] args)
        {
                Book book1 = new Book();
		Book book2 = new Book();
		Book book3 = new Book();
		String str;
		int r;
		
		Scanner scan = new Scanner(System.in);

		System.out.print("Enter Book1's name : ");
		str = scan.nextLine();
		book1.setName(str);

		System.out.print("Enter Book1's auther : ");
		str = scan.nextLine();
		book1.setAuther(str);

		System.out.print("Enter Book1's press : ");
		str = scan.nextLine();
		book1.setPress(str);

		System.out.print("Enter Book1's copy right year : ");
		r = scan.nextInt();
		book1.setRightYear(r);

		System.out.print("Enter Book1's copy right month : ");
		r = scan.nextInt();
		book1.setRightMonth(r);
		
		System.out.print("Enter Book1's copy right date : ");
		r = scan.nextInt();
		book1.setRightDate(r);

		
		scan.nextLine();


		System.out.print("Enter Book2's name : ");
		str = scan.nextLine();
		book2.setName(str);
		 
		System.out.print("Enter Book2's auther : ");
		str = scan.nextLine();
		book2.setAuther(str);
		
		System.out.print("Enter Book2's press : ");
		str = scan.nextLine();
		book2.setPress(str);

		System.out.print("Enter Book2's copy right year : ");
		r = scan.nextInt();
		book2.setRightYear(r);
		 
		System.out.print("Enter Book2's copy right month : ");
		r = scan.nextInt();
		book2.setRightMonth(r);
		
		System.out.print("Enter Book2's copy right date : ");
		r = scan.nextInt();
		book2.setRightDate(r);

		
		scan.nextLine();


		System.out.print("Enter Book3's name : ");
		str = scan.nextLine();
		book3.setName(str);
		 
		System.out.print("Enter Book3's auther : ");
		str = scan.nextLine();
		book3.setAuther(str);
		
		System.out.print("Enter Book3's press : ");
		str = scan.nextLine();
		book3.setPress(str);

		System.out.print("Enter Book3's copy right year : ");
		r = scan.nextInt();
		book3.setRightYear(r);
		 
		System.out.print("Enter Book3's copy right month : ");
		r = scan.nextInt();
		book3.setRightMonth(r);
		
		System.out.print("Enter Book3's copy right date : ");
		r = scan.nextInt();
                book3.setRightDate(r);


		scan.nextLine();


		System.out.println("Information of book1 : ");
		System.out.println(book1.toString());

		System.out.println("Information of book2 : ");
		System.out.println(book2.toString());

		System.out.println("Information of book3 : ");
		System.out.println(book3.toString());

        }
}
