
//***********************************************
//
//     Filename: chengxu2_9.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-15 16:49:51
//Last Modified: 2019-09-15 16:49:51
//***********************************************
import java.util.Scanner;
public class chengxu2_9
{
        public static void main(String[] args)
        {
                int miles;
		double gallons, mpg;
		Scanner scan = new Scanner (System.in);
		System.out.print ( "Enter the number of miles: ");
		miles = scan.nextInt();
		System.out.print ( "Enter the gallons of fuel used: ");
		gallons = scan.nextDouble();
		mpg = miles / gallons;
		System.out.println ("Miles Per Gallon: " + mpg);
        }
}
