package MyHobby;

public class GraphEdge {
    int value;
    GraphRoot target;
    GraphEdge next;

    public GraphEdge(int val, GraphRoot tar){
        value = val;
        target = tar;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public GraphRoot getTarget() {
        return target;
    }

    public void setTarget(GraphRoot target) {
        this.target = target;
    }

    public GraphEdge getNext() {
        return next;
    }

    public void setNext(GraphEdge next) {
        this.next = next;
    }
}
