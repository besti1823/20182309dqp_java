public class Student implements People {
    @Override
    public String work() {
        return "study";
    }

    @Override
    public String home() {
        return "apartment";
    }

    @Override
    public String eat() {
        return "dining hall";
    }
}
