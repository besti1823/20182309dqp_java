
//        ****************************************************
// Slogan. java
//        Java Foundations
//        1/
// Represents a single slogan or motto
// //********************************************************************
public class Slogan{
    private String phrase;
    private static int count = 0;
//---
// Constructor: sets up the slogan and increments the number of
// instances created.
    public Slogan (String str) {
        phrase = str;
        count++;
    }
// Returns this slogan as a string.
    public String toString(){
        return phrase;
    }
//----------------- _o F
//instancee of this class that have been
// Returns the number of '
// created.

    public static int getCount () {
        return count;
    }
}
