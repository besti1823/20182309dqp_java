
//***********************************************
//
//     Filename: pp2_6.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-15 18:08:25
//Last Modified: 2019-09-15 18:08:25
//***********************************************
import java.util.Scanner;
public class pp2_6
{
        public static void main(String[] args)
        {
                int h,m,s,sums;
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter hours:");
		h=scan.nextInt();
		System.out.print("Enter minutes:");
		m=scan.nextInt();
		System.out.print("Enter seconds");
		s=scan.nextInt();
		sums=h*3600+m*60+s;
		System.out.println("Totally "+sums+" seconds");
        }
}
