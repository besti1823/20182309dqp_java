package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.io.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AutoCompleteTextView put1 = findViewById(R.id.text1);
        AutoCompleteTextView put2 = findViewById(R.id.text2);
        AutoCompleteTextView put3 = findViewById(R.id.text3);
        AutoCompleteTextView put4 = findViewById(R.id.text4);

        ArrayList al = new ArrayList();
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(4);
        al.add(5);
        al.add(6);
        al.add(7);
        al.add(8);
        put1.setText(al.toString());

        al.remove(5);
        put2.setText(al.toString());

        al.set(3,9);
        put3.setText(al.toString());

        Collections.reverse(al);
        put4.setText(al.toString());
    }
}

