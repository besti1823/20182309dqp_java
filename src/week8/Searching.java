import org.w3c.dom.ls.LSOutput;

public class Searching {
    public static Comparable linearSearch(Comparable[] data, Comparable target) {
        //线性查找
        Comparable result = null;
        int index = 0;

        while (result == null && index < data.length) {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }

        return result;
    }

    public static Comparable binarySearch(Comparable[] data1, Comparable target) {
        //折半查找
        //复制一个数组，避免改变原数组的顺序
        Comparable[] data = data1;
        Sorting.bubbleSort(data);

        Comparable result = null;
        int first = 0, last = data.length - 1, mid;
        while (result == null && first <= last) {
            mid = (first + last) / 2;
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid - 1;
        }

        return result;
    }
}
