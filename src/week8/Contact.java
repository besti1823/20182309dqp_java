import java.util.Arrays;

public class Contact {
    private final int MAX = 100;
    private int i = 0;
    Person[] per = new Person[MAX];

    public void Add(String name, String number){
        per[i] = new Person(name,number);
        i++;
        sort();
    }

    public void sort(){
        int min;

        for(int index = 0; index < i - 1; index++){
            min = index;
            for(int scan = index+1; scan < i; scan++)
                if(per[scan].getNumber().compareTo(per[min].getNumber()) > 0)
                    min = scan;

            Person temp;
            temp = per[min];
            per[min] = per[index];
            per[index] = temp;
        }
    }

    @Override
    public String toString() {
        return "Contact{" +
                "per=" + Arrays.toString(per) +
                '}';
    }

    public class Person{
        private String number;
        private String name;

        public Person(String name, String number) {
            this.number = number;
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "number='" + number + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
