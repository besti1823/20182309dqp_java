import java.util.Iterator;

public class SearchingRun {
    public static void main(String[] args) {
        Comparable[] a = new Comparable[12];

        a[0] = 19;
        a[1] = 14;
        a[2] = 23;
        a[3] = 1;
        a[4] = 68;
        a[5] = 20;
        a[6] = 84;
        a[7] = 27;
        a[8] = 55;
        a[9] = 11;
        a[10] = 10;
        a[11] = 79;

        HashTablelink htl = new HashTablelink(12);
        htl.insert(new LinkHash(19));
        htl.insert(new LinkHash(14));
        htl.insert(new LinkHash(23));
        htl.insert(new LinkHash(1));
        htl.insert(new LinkHash(68));
        htl.insert(new LinkHash(20));
        htl.insert(new LinkHash(84));
        htl.insert(new LinkHash(27));
        htl.insert(new LinkHash(55));
        htl.insert(new LinkHash(11));
        htl.insert(new LinkHash(10));
        htl.insert(new LinkHash(79));

        HashTable ht = new HashTable(12);
        ht.insert(new DataItem(19));
        ht.insert(new DataItem(14));
        ht.insert(new DataItem(23));
        ht.insert(new DataItem(1));
        ht.insert(new DataItem(68));
        ht.insert(new DataItem(20));
        ht.insert(new DataItem(84));
        ht.insert(new DataItem(27));
        ht.insert(new DataItem(55));
        ht.insert(new DataItem(11));
        ht.insert(new DataItem(10));
        ht.insert(new DataItem(79));

        BinarySortTree<Integer> tree = new BinarySortTree<Integer>();
        tree.add(19);
        tree.add(14);
        tree.add(23);
        tree.add(1);
        tree.add(68);
        tree.add(20);
        tree.add(84);
        tree.add(27);
        tree.add(55);
        tree.add(11);
        tree.add(10);
        tree.add(79);

        System.out.println("线性查找："+Searching.linearSearch(a,23));//线性查找
        System.out.println("折半查找："+Searching.binarySearch(a,27));//折半查找

        System.out.print("二叉排序：");
        Iterator<Integer> it = tree.itrator();
        while (it.hasNext()) {
            System.out.print(it.next()+" ");
        }

        System.out.println();
        htl.displayTable();//链地址生成的哈希表

        LinkHash find = htl.find(55);
        if(find == null) System.out.println("没找到");
        else System.out.println("找到了");

        System.out.println();
        ht.displayTable();//线性探测法生成的哈希表
        ht.find(11);
        if(ht == null) System.out.println("没找到");
        else System.out.println("找到了");
    }
}
