import java.io.*;
import java.math.BigInteger;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class Password implements Encryptable2{
    private String message;
    private boolean encrypted;
    private BigInteger e;
    private BigInteger n;

    public Password(String message) {
        this.message = message;
        encrypted=false;
    }

    @Override
    public void encrypt()throws IOException,ClassNotFoundException{
        if(!encrypted){
            FileInputStream f=new FileInputStream("key_RSA_public.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPublicKey pbk=(RSAPublicKey)b.readObject( );
            e=pbk.getPublicExponent();
            n=pbk.getModulus();

            byte ptext[]=message.getBytes("UTF8");
            BigInteger m=new BigInteger(ptext);

            BigInteger c=m.modPow(e,n);
            message=c.toString();
            encrypted=true;
        }
    }

    @Override
    public String decrypt()throws IOException,ClassNotFoundException {
        if(encrypted){
            BigInteger c=new BigInteger(message);

            FileInputStream f=new FileInputStream("key_RSA_private.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPrivateKey prk=(RSAPrivateKey)b.readObject( );
            BigInteger d=prk.getPrivateExponent();

            BigInteger n=prk.getModulus();
            BigInteger m=c.modPow(d,n);
            byte[] mt=m.toByteArray();
            message="";
            for(int index=0;index<mt.length;index++){
                message+=(char)mt[index];
            }
            encrypted=false;
        }
        return message;
    }

    public String toString(){
        return message;
    }
}
