import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface Encryptable2 {
    public void encrypt() throws UnsupportedEncodingException, IOException, ClassNotFoundException;
    public String decrypt() throws IOException, ClassNotFoundException;
}
