public class Staff2 {
    private StaffMember2[] stafflist;

    public Staff2(){
        stafflist=new StaffMember2[6];

        stafflist[0]=new Executive2("Tony","123 Min Line","555-0590","123-45-6789",2423.07);
        stafflist[1]=new Employee2("Paulie","456 Off Line","555-0101","987-65-4321",123.15);
        stafflist[2]=new Employee2("Vito","789 Off Rocker","555-0000","010-20-3040",1169.23);
        stafflist[3]=new Hourly2("Michael","678 Fifth Ave","555-0690","958-47-3625",10.55);
        stafflist[4]=new Volunteer2("Adrianna","987 Babe Blvd","555-8374");
        stafflist[5]=new Volunteer2("Benny","321 Dud Lane","555-7282");

        ((Executive2)stafflist[0]).awardBonus(500.0);
        ((Hourly2)stafflist[3]).addHours(40);

    }

    public void payday(){
        double amount;
        for(int count=0;count<stafflist.length;count++){
            System.out.println(stafflist[count]);
            amount=stafflist[count].pay();

            if(amount==0.0){
                System.out.println("Thanks!");
            }
            else
                System.out.println("Paid"+amount);
        }
        System.out.println("-------------------------------------------");
    }

    public void goTravel(){
        int a;
        for(int count=0;count<stafflist.length;count++){
            System.out.println(stafflist[count]);
            a=stafflist[count].travel();
            switch(a){
                case 1:
                    System.out.println("travel here");
                    break;
                case 2:
                    System.out.println("travel there");
                    break;
                case 3:
                    System.out.println("travel anywhere");
                    break;
            }
            System.out.println("----------------------------------");
        }
    }
}
