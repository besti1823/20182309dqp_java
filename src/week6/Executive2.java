public class Executive2 extends Employee2{
    private double bonus;
    public Executive2(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone, socialSecurityNumber, payRate);
        this.bonus = 0;
        this.rank=3;
    }

    public void awardBonus(double execBonus){
        bonus=execBonus;
    }

    public double pay(){
        double payment=super.pay()+bonus;
        bonus=0;
        return payment;
    }

    public int travel(){
        return rank;
    }
}
