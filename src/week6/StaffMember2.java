abstract public class StaffMember2 {
    protected String name;
    protected String address;
    protected String phone;
    protected int rank;

    public StaffMember2(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Name:"+name+"\n"+"Address:"+address+"\n"+"Phone:"+phone+"\n";
    }

    public abstract double pay();
    public abstract int travel();
}
