import java.util.Scanner;

public class TooLongTest2 {
    public static void main(String[] args) throws StringTooLongException {
        Scanner scan=new Scanner(System.in);
        String a=null;
        for(;;){
            try{
                System.out.println("Enter (stop at \"DONE\"):");
                a=scan.nextLine();
                if(a.length()>=20){
                    throw new StringTooLongException("Too much Char");
                }
            }
            catch (StringTooLongException e){
                System.out.println(e);
            }

            if(!a.equals("DONE"))break;
        }
    }
}
