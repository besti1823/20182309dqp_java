public class Employee2 extends  StaffMember2{
    protected String socialSecurityNumber;
    protected double payRate;
    protected int rank;

    public Employee2(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone);
        this.socialSecurityNumber = socialSecurityNumber;
        this.payRate = payRate;
        this.rank=2;
    }

    @Override
    public String toString() {
        String str=super.toString();
        str+="\nSocial Security Number:"+socialSecurityNumber;
        return str;
    }

    public double pay(){
        return payRate;
    }

    public int travel(){
        return rank;
    }
}
