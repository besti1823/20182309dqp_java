import java.util.*;

public class TaskTest2 {
    public static void main(String[] args) {
        Task[] list=new Task[4];
        int[] a=new int[4];
        List<Task> tasklist=new ArrayList<>();

        Task task=new Task("");
        list[0]=new Task("do1");
        list[1]=new Task("do2");
        list[2]=new Task("do3");
        list[3]=new Task("do4");

        for(int i=0;i<4;i++){
            tasklist.add(list[i]);
        }

        Scanner in=new Scanner(System.in);
        System.out.println("the bigger the prior:");
        for(int i=0;i<4;i++){
            a[i]=in.nextInt();
        }

        task.set(list,a);
        for(Task c:list){
            System.out.println(c.getName()+"'s Priority:"+c.getPriority());
        }

        Arrays.sort(list);
        for(Task c:list){
            System.out.println(c.getName()+"'s Priority:"+c.getPriority());
        }

    }
}
