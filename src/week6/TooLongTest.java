import java.util.Scanner;

public class TooLongTest {
    public static void main(String[] args) throws StringTooLongException {
        Scanner scan=new Scanner(System.in);
        String a=null;
        for(;;){
            System.out.println("Enter (stop at \"DONE\"):");
            a=scan.nextLine();
            if(a.length()>=20){
                throw new StringTooLongException("Too much Char");
            }
            if(!a.equals("DONE"))break;
        }
    }
}
