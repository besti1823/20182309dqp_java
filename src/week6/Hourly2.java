public class Hourly2 extends Employee2 {
    private int hoursWorked;

    public Hourly2(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone, socialSecurityNumber, payRate);
        hoursWorked=0;
        this.rank=1;
    }

    public void addHours(int moreHours){
        hoursWorked+=moreHours;
    }

    public double pay(){
        double payment=payRate+hoursWorked;
        hoursWorked=0;
        return payment;
    }

    public String toString(){
        String result = super.toString();

        result+="\nCurrent hours:"+hoursWorked;
        return result;
    }

    public int travel(){
        return rank;
    }
}
