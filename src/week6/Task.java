public class Task implements Priority,Comparable<Task> {
    private int priority;
    private String name;

    public Task(String name) {
        this.name = name;
    }

    @Override
    public void setPriority(int n) {
        this.priority=n;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public void set(Task[] t,int[] n){
        for(int i=0;i<t.length;i++){
            t[i].setPriority(n[i]);
        }
    }

    public String getName() {
        return this.name;
    }
    @Override
    public int compareTo(Task c) {
        if(this.priority>c.priority)
            return 1;
        else if(this.priority<c.priority)
            return -1;
        else
            return 0;
    }

}
