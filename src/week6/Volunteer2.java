public class Volunteer2 extends StaffMember2{
    private int rank=2;

    public Volunteer2(String name, String address, String phone) {
        super(name, address, phone);
    }

    public double pay(){
        return 0.0;
    }
    public int travel(){
        return rank;
    }
}
