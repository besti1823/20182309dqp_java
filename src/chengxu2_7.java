
//***********************************************
//
//     Filename: chengxu2_7.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-15 16:27:37
//Last Modified: 2019-09-15 16:27:37
//***********************************************

public class chengxu2_7
{
        public static void main(String[] args)
        {
                final int BASE = 32;
		final double CONVERSION_FACTOR = 9.0 / 5.0;
		double fahrenheitTemp;
		int celsiusTemp = 24;//value to convert
		fahrenheitTemp = celsiusTemp * CONVERSION_FACTOR + BASE;
		System.out.println ( "Celsius Temperature: " + celsiusTemp) ;
		System.out.println ("Fahrenheit Equivalent: " + fahrenheitTemp) ;
        }
}
