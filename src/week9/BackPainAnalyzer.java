package chapter16;

import javafoundations.exceptions.EmptyCollectionException;

public class BackPainAnalyzer
{
    //-----------------------------------------------------------------
// Asks questions of the user to diagnose a medical problem.
//-----------------------------------------------------------------
    public static void main (String[] args) throws EmptyCollectionException {
        BackPainExpert expert = new BackPainExpert();
        expert.diagnose();
    }
}