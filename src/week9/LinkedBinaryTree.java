public class LinkedBinaryTree<T>
{
    protected BTNode<T> root;
    //-----------------------------------------------------------------
// Creates an empty binary tree.
//-----------------------------------------------------------------
    public LinkedBinaryTree()
    {
        root = null;
    }
    //-----------------------------------------------------------------
// Creates a binary tree with the specified element as its root.
//-----------------------------------------------------------------
    public LinkedBinaryTree (T element)
    {
        root = new BTNode<T>(element);
    }
    //-----------------------------------------------------------------
// Creates a binary tree with the two specified subtrees.
//-----------------------------------------------------------------
    public LinkedBinaryTree (T element, LinkedBinaryTree<T> left,
                             LinkedBinaryTree<T> right)
    {
        root = new BTNode<T>(element);
        if(left != null)
            root.setLeft(left.root);
        if(right != null)
            root.setRight(right.root);
    }
    //-----------------------------------------------------------------
// Returns the element stored in the root of the tree. Throws an
// EmptyCollectionException if the tree is empty.
//-----------------------------------------------------------------
    public T getRootElement() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get root operation "
                    + "failed. The tree is empty.");
        return root.getElement();
    }
    //-----------------------------------------------------------------
// Returns the left subtree of the root of this tree.
//-----------------------------------------------------------------
    public LinkedBinaryTree<T> getLeft() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();
        return result;
    }

    public LinkedBinaryTree<T> getRight()throws EmptyCollectionException{
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();
        return result;
    }
    //-----------------------------------------------------------------
// Returns the element in this binary tree that matches the
// specified target. Throws a ElementNotFoundException if the
// target is not found.
//-----------------------------------------------------------------
    public T find (T target) throws ElementNotFoundException {
        BTNode<T> node = null;
        if (root != null)
            node = root.find(target);
        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");
        return node.getElement();
    }
    //-----------------------------------------------------------------
// Returns the number of elements in this binary tree.
//-----------------------------------------------------------------
    public int size()
    {
        int result = 0;
        if (root != null)
            result = root.count();
        return result;
    }

    public boolean isEmpty() {
        return (root == null);
    }
    //-----------------------------------------------------------------
// Populates and returns an iterator containing the elements in
// this binary tree using an inorder traversal.
//-----------------------------------------------------------------
    public ArrayIterator<T> inorder()
    {
        ArrayIterator<T> iter = new ArrayIterator<T>();
        if (root != null)
            root.inorder (iter);
        return iter;
    }

    public ArrayIterator<T> preorder() {
        ArrayIterator<T> iter = new ArrayIterator<T>();
        if(root != null)
            root.preorder(iter);
        return iter;
    }

    public ArrayIterator<T> postorder() {
        ArrayIterator<T> iter = new ArrayIterator<T>();
        if(root != null)
            root.postorder(iter);
        return iter;
    }
    //-----------------------------------------------------------------
// Populates and returns an iterator containing the elements in
// this binary tree using a levelorder traversal.
//-----------------------------------------------------------------
    public ArrayIterator<T> levelorder() throws EmptyCollectionException {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayIterator<T> iter = new ArrayIterator<T>();
        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();
                iter.add (current.getElement());
                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            } }
        return iter;
    }
    //-----------------------------------------------------------------
// Satisfies the Iterable interface using an inorder traversal.
//-----------------------------------------------------------------
    public ArrayIterator<T> iterator()
    {
        return inorder();
    }

    public boolean contains (T target){
        return contains(target, root);
    }

    private boolean contains (T target,BTNode<T> root) {
        if(root == null)
            return false;
        else if (root.getElement() == target)
            return true;
        else {
            BTNode<T> node1 = root.getLeft();
            BTNode<T> node2 = root.getRight();
            boolean rpy;

            if(contains(target, node1))
                return true;
            else if(contains(target, node2))
                return true;
            else
                return false;
        }
    }

    public String toString() {
        String rpy = null;
        return preorder().toString();
    }

}