package test;

import MyHobby.*;

public class Graph {
    public static void main(String[] args) {
        GraphRoot v0 = new GraphRoot(0);
        GraphRoot v1 = new GraphRoot(1);
        GraphRoot v2 = new GraphRoot(2);
        GraphRoot v3 = new GraphRoot(3);
        GraphRoot v4 = new GraphRoot(4);

        GraphEdge e1 = new GraphEdge(5,v1);
        GraphEdge e2 = new GraphEdge(7,v3);
        GraphEdge e3 = new GraphEdge(4,v2);
        GraphEdge e4 = new GraphEdge(8,v0);
        GraphEdge e5 = new GraphEdge(2,v4);
        GraphEdge e6 = new GraphEdge(6,v2);
        GraphEdge e7 = new GraphEdge(3,v3);

        v0.setNext(e1);
        e1.setNext(e2);

        v1.setNext(e3);

        v2.setNext(e4);
        e4.setNext(e5);

        v3.setNext(e6);

        v4.setNext(e7);

        int count = 0;
        GraphEdge temp = v0.getNext();
        for(;;){
            if(temp != null){
                temp = temp.getNext();
                count++;
            }
            else {
                System.out.println("v0出度：" + count);
                break;
            }
        }

        count = 0;
        temp = v1.getNext();
        for(;;){
            if(temp != null){
                temp = temp.getNext();
                count++;
            }
            else {
                System.out.println("v1出度：" + count);
                break;
            }
        }

        count = 0;
        temp = v2.getNext();
        for(;;){
            if(temp != null){
                temp = temp.getNext();
                count++;
            }
            else {
                System.out.println("v2出度：" + count);
                break;
            }
        }

        count = 0;
        temp = v3.getNext();
        for(;;){
            if(temp != null){
                temp = temp.getNext();
                count++;
            }
            else {
                System.out.println("v3出度：" + count);
                break;
            }
        }

        count = 0;
        temp = v4.getNext();
        for(;;){
            if(temp != null){
                temp = temp.getNext();
                count++;
            }
            else {
                System.out.println("v4出度：" + count);
                break;
            }
        }
    }
}
