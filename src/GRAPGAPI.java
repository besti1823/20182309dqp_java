package MyHobby;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class GRAPGAPI  {
    public class Vertex implements Comparable<Vertex>{

        private String name;

        private int minPath;

        private int anotherIDinminEdge;

        private boolean isMarked;

        public Vertex(String name){
            this.name = name;
            this.minPath = Integer.MAX_VALUE;
            this.anotherIDinminEdge=-1;
            this.setMarked(false);
        }
        public Vertex(String name, int path){
            this.name = name;
            this.minPath = path;
            this.setMarked(false);
        }
        public int getAnotherIDinminEdge() {
            return anotherIDinminEdge;
        }
        public void setAnotherIDinminEdge(int anotherIDinminEdge) {
            this.anotherIDinminEdge = anotherIDinminEdge;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public int getPath() {
            return minPath;
        }

        public void setPath(int path) {
            this.minPath = path;
        }

        boolean isMarked(){
            return this.isMarked;

        }

        void setMarked(boolean flag){
            this.isMarked=flag;
        }

        @Override
        public int compareTo(Vertex o) {
            return o.minPath > minPath?-1:1;
        }
    }


    public class Graph {

        private List<Vertex> vertexs;

        private int[][] edges;

        private List<Vertex> topVertexs;

        private Queue<Vertex> unVisited;

        private int[][] minTree;


        public Graph(List<Vertex> vertexs, int[][] edges) {
            this.vertexs = vertexs;
            this.topVertexs=new ArrayList<Vertex>();
            this.edges = edges;
            this.minTree=new int[this.vertexs.size()][this.vertexs.size()];
            initUnVisited();
        }

        public void  DFS(String vertexName){
            int id=getIdOfVertexName(vertexName);
            if(id==-1)return;
            vertexs.get(id).setMarked(true);
            System.out.print(vertexs.get(id).getName());
            List<Vertex> neighbors = getNeighbors(vertexs.get(id));
            for(int i=0;i<neighbors.size();i++){
                if(!neighbors.get(i).isMarked()){
                    DFS(neighbors.get(i).getName());
                }
            }
        }

        public void BFS(String vertexName){
            int startID=getIdOfVertexName(vertexName);
            if(startID==-1) return;
            List<Vertex> q=new ArrayList<Vertex>();
            q.add(vertexs.get(startID));
            vertexs.get(startID).setMarked(true);
            while(!q.isEmpty()){
                Vertex curVertex=q.get(0);
                q.remove(0);
                System.out.print(curVertex.getName());
                List<Vertex> neighbors = getNeighbors(curVertex);
                for(int i=0;i<neighbors.size();i++){
                    if(!neighbors.get(i).isMarked()){
                        neighbors.get(i).setMarked(true);
                        q.add(neighbors.get(i));
                    }
                }

            }

        }

        public void tuopusort(){

        }

        public int[][] getMinTree(){
            initMinTree();
            while(!allVisited()){
                Vertex vertex = vertexs.get(getNotMarkedMinVertex());
                System.out.println(""+vertex.getName());

                vertex.setMarked(true);

                List<Vertex> neighbors = getNeighbors(vertex);
                System.out.println(""+neighbors.size());

                updateMinEdge(vertex, neighbors);
            }
            System.out.println("");
            setMinTree();

            return minTree;
        }

        public void setMinTree(){
            for(int i=0;i<vertexs.size();i++){
                if(vertexs.get(i).getAnotherIDinminEdge()!=-1){
                    minTree[getIdOfVertexName(vertexs.get(i).getName())][vertexs.get(i).getAnotherIDinminEdge()]=
                            edges[getIdOfVertexName(vertexs.get(i).getName())][vertexs.get(i).getAnotherIDinminEdge()];
                    minTree[vertexs.get(i).getAnotherIDinminEdge()][getIdOfVertexName(vertexs.get(i).getName())]=
                            edges[vertexs.get(i).getAnotherIDinminEdge()][getIdOfVertexName(vertexs.get(i).getName())];
                }
            }
        }

        public void initMinTree(){
            for(int i=0;i<this.vertexs.size();i++){
                for(int j=0;j<this.vertexs.size();j++){
                    this.minTree[i][j]=Integer.MAX_VALUE;
                }

            }
        }

        public void  updateMinEdge(Vertex vertex, List<Vertex> neighbors){
            if(!isInGraph(vertex)){
                System.out.println("");
                return ;
            }

            for(Vertex neighbor: neighbors){
                int distance = edges[getIdOfVertexName(neighbor.getName())][getIdOfVertexName(vertex.getName())];
                if(neighbor.getAnotherIDinminEdge()==-1){
                    neighbor.setAnotherIDinminEdge(getIdOfVertexName(vertex.getName()));
                    System.out.println(neighbor.getName()+" =====> "+vertex.getName()+edges[neighbor.getAnotherIDinminEdge()][getIdOfVertexName(neighbor.getName())]);
                }
                else if(distance <  edges[getIdOfVertexName(neighbor.getName())][neighbor.getAnotherIDinminEdge()]){
                    neighbor.setAnotherIDinminEdge(getIdOfVertexName(vertex.getName()));
                    System.out.println(neighbor.getName()+" =====> "+vertex.getName()+edges[neighbor.getAnotherIDinminEdge()][getIdOfVertexName(neighbor.getName())]);
                }
            }
        }

        public void topSort(){
            int[][] tmpEdges=edges;
            int IDofNullPreVertex=getNullPreVertexID(tmpEdges);
            while(IDofNullPreVertex!=-1){
                vertexs.get(IDofNullPreVertex).setMarked(true);
                topVertexs.add(vertexs.get(IDofNullPreVertex));

                for(int j=0;j<this.vertexs.size();j++){
                    if(tmpEdges[IDofNullPreVertex][j]!=Integer.MAX_VALUE){
                        tmpEdges[IDofNullPreVertex][j]=Integer.MAX_VALUE;
                    }
                }
                IDofNullPreVertex=getNullPreVertexID(tmpEdges);
            }

        }

        public int getNullPreVertexID(int[][] tmpEdges){
            int resultId=-1;
            for(int j=0;j< this.vertexs.size();j++){
                boolean flag=false;
                for(int i=0;i< this.vertexs.size();i++){
                    if(tmpEdges[i][j]!=Integer.MAX_VALUE){
                        flag=true;
                    }
                }
                if(!flag&&!vertexs.get(j).isMarked()){
                    resultId=j;
                    break;
                }
            }
            return resultId;
        }

        public void search(){
            while(!unVisited.isEmpty()){
                Vertex vertex = unVisited.element();

                vertex.setMarked(true);

                List<Vertex> neighbors = getNeighbors(vertex);

                updatesDistance(vertex, neighbors);
                pop();
            }
            System.out.println("???·??");
        }

        private void updatesDistance(Vertex vertex, List<Vertex> neighbors){

            if(!isInGraph(vertex)){
                System.out.println("");
                return ;
            }
            for(Vertex neighbor: neighbors){
                updateDistance(vertex, neighbor);
            }
        }

        private void updateDistance(Vertex vertex, Vertex neighbor){

            if(!isInGraph(vertex)||!isInGraph(neighbor)){
                System.out.println("");
                return ;
            }
            int distance = getDistance(vertex, neighbor) + vertex.getPath();
            if(distance < neighbor.getPath()){
                neighbor.setPath(distance);
            }
        }

        private void initUnVisited() {
            unVisited = new PriorityQueue<Vertex>();
            for (Vertex v : vertexs) {
                unVisited.add(v);
            }
        }

        private void pop() {
            unVisited.poll();
        }

        private int getDistance(Vertex source, Vertex destination) {
            if(!isInGraph(source)||!isInGraph(destination)){
                System.out.println("");
                return -1;
            }



            int sourceIndex = vertexs.indexOf(source);
            int destIndex = vertexs.indexOf(destination);
            return edges[sourceIndex][destIndex];
        }

        public List<Vertex> getNeighbors(Vertex v) {

            if(!isInGraph(v)){
                System.out.println("");
                return null;
            }
            List<Vertex> neighbors = new ArrayList<Vertex>();
            int position = vertexs.indexOf(v);
            Vertex neighbor = null;
            int distance;
            for (int i = 0; i < vertexs.size(); i++) {
                if (i == position) {
                    continue;
                }
                distance = edges[position][i];
                if (distance < Integer.MAX_VALUE) {

                    neighbor = getVertex(i);
                    if (!neighbor.isMarked()) {

                        neighbors.add(neighbor);
                    }
                }
            }
            return neighbors;
        }

        private Vertex getVertex(int index) {
            if(index<0||index>vertexs.size()+1){
                System.out.println(""+index+"");
                return null;
            }
            return vertexs.get(index);
        }

        public void printGraph() {
            int verNums = vertexs.size();
            for (int row = 0; row < verNums; row++) {
                for (int col = 0; col < verNums; col++) {
                    if(Integer.MAX_VALUE == edges[row][col]){
                        System.out.print("∞");
                        System.out.print(" ");
                        continue;
                    }
                    System.out.print(edges[row][col]);
                    System.out.print(" ");
                }
                System.out.println();
            }
        }

        public void dijstl(String node){

        }

        public boolean allVisited(){
            boolean flag=true;
            for(int i=0;i<vertexs.size();i++){
                if(!vertexs.get(i).isMarked())
                    flag=false;
            }
            return flag;
        }

        public void printTopSort(){
            for(int i=0;i<this.topVertexs.size();i++){
                System.out.print(this.topVertexs.get(i).getName()+" ");
            }
            System.out.println();
        }

        public int getIdOfVertexName(String name){
            int id=-1;
            for(int i=0;i<vertexs.size();i++){
                if(vertexs.get(i).getName().equals(name))
                    id=i;
            }
            return id;
        }

        public int getNotMarkedMinVertex(){
            int min=10000;
            int id=0;
            for(int i=0;i<vertexs.size();i++){
                if(!vertexs.get(i).isMarked()&&vertexs.get(i).getAnotherIDinminEdge()!=-1){
                    if(min>edges[getIdOfVertexName(vertexs.get(i).getName())][vertexs.get(i).getAnotherIDinminEdge()]){
                        min=edges[getIdOfVertexName(vertexs.get(i).getName())][vertexs.get(i).getAnotherIDinminEdge()];
                        id=i;

                    }
                }
            }
            return id;
        }

        public void clearGraph(){
            for(Vertex vertex:vertexs){
                vertex.setMarked(false);
                vertex.setAnotherIDinminEdge(-1);
            }

        }

        private boolean isInGraph(Vertex v){
            for(Vertex vertex:vertexs){
                if(vertex.getName().equals(v.getName()))
                    return true;
            }
            return false;
        }


    }


    public static void main(String[] args) {
        GRAPGAPI main=new GRAPGAPI();

        List<Vertex> vertexs = new ArrayList<Vertex>();
        Vertex a = main.new Vertex("A",0);//0??????????????·???????0
        Vertex b = main.new Vertex("B");
        Vertex c = main.new Vertex("C");
        Vertex d = main.new Vertex("D");
        Vertex e = main.new Vertex("E");
        Vertex f = main.new Vertex("F");
        vertexs.add(a);
        vertexs.add(b);
        vertexs.add(c);
        vertexs.add(d);
        vertexs.add(e);
        vertexs.add(f);
        int[][] edges = {
                {Integer.MAX_VALUE,2,3,Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE},
                {6,Integer.MAX_VALUE,2,5,Integer.MAX_VALUE,Integer.MAX_VALUE},
                {3,2,Integer.MAX_VALUE,3,4,Integer.MAX_VALUE},
                {Integer.MAX_VALUE,5,3,Integer.MAX_VALUE,5,3},
                {Integer.MAX_VALUE,Integer.MAX_VALUE,4,5,Integer.MAX_VALUE,5},
                {2,Integer.MAX_VALUE,Integer.MAX_VALUE,3,5,Integer.MAX_VALUE}

        };
        Graph graph = main.new Graph(vertexs, edges);


        System.out.println("邻接矩阵：");
        graph.printGraph();

        System.out.println("DFS节点E");
        graph.DFS("E");
        graph.clearGraph();
        System.out.println();

        System.out.println("BFS节点E");
        graph.BFS("E");
        graph.clearGraph();
        System.out.println();

        System.out.println("拓扑排序：");
        graph.tuopusort();

        System.out.println("最小生成树");
        int[][] minTree=graph.getMinTree();
        for(int i=0;i<vertexs.size();i++){
            for(int j=0;j<vertexs.size();j++){
                if(minTree[i][j]==Integer.MAX_VALUE)
                    minTree[i][j]=0;
                System.out.print(minTree[i][j]+" ");
            }
            System.out.println();
        }
        graph.clearGraph();
        System.out.println("E的单源最短路径");
        graph.dijstl("E");
    }
}
