
//***********************************************
//
//     Filename: shiyan2_1.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-16 16:11:08
//Last Modified: 2019-09-16 16:11:08
//***********************************************
import java.util.Scanner;
public class shiyan2_1
{
	public static double calculator(double a,char op,double b)
	{
		double ans=0;

		switch(op)
		{
			case '+':
				ans=a+b;
				break;
		        case '-':
		                ans=a-b;
		                break;
		        case '*':
		                ans=a*b;
		                break;
		        case '/':
		                ans=a/b;
		                break;
		        case '%':
		                ans=a%b;
		                break;
		}
		return ans;
	}
}
