import java.util.Scanner;

public class MyLinkRun {
    public static void main(String[] args) {
        MyLink linkint = new MyLink();
        Scanner scan = new Scanner(System.in);
        int n,i,index;

        System.out.println("输入整数（以0结尾）：");
        for (;;){
            n=scan.nextInt();
            if(n==0)break;
            linkint.addNode(n);
        }
        System.out.println("输入完毕！");
        //输出
        linkint.printList();

        //插入节点
        System.out.print("插入在第几位后:");
        index = scan.nextInt();
        System.out.print("插入整数：");
        n = scan.nextInt();
        linkint.insertNode(index,n);
        //输出
        linkint.printList();

        //删除节点
        System.out.println("删除第几位：");
        index = scan.nextInt();
        linkint.deleteNode(index);
        //输出
        linkint.printList();

        //冒泡排序
        linkint.bubbleSort();
        //输出
        System.out.println("排序后：");
        linkint.printList();
    }
}
