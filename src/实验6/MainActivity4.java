package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import java.util.Scanner;
import java.io.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)throws IOException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyLink linkint = new MyLink();
        Scanner scan = new Scanner(System.in);
        int n,i,index,a,b;

        //写文件
        File file = new File("Number.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'7','5'};
        outputStream1.write(test1);

        //读文件
        InputStream inputStream1 = new FileInputStream(file);
        a = (char) inputStream1.read()-48;
        b = (char) inputStream1.read()-48;

        for (;;){
            n=scan.nextInt();
            if(n==0)break;
            linkint.addNode(n);
        }
        //输出
        linkint.printList();
        //插入节点
        linkint.insertNode(4,a);
        linkint.insertNode(0,b);
        //输出
        linkint.printList();

        //删除节点
        linkint.deleteNode(5);
        //输出
        linkint.printList();
        System.out.println("长度"+linkint.length());

    }
}

