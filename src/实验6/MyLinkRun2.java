import java.util.Scanner;
import java.io.*;

public class MyLinkRun2 {
    public static void main(String[] args) throws IOException {
        MyLink linkint = new MyLink();
        Scanner scan = new Scanner(System.in);
        int n,i,index,a,b;

        //写文件
        File file = new File("Number.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'7','5'};
        outputStream1.write(test1);

        //读文件
        InputStream inputStream1 = new FileInputStream(file);
        a = (char) inputStream1.read()-48;
        b = (char) inputStream1.read()-48;

        System.out.println("输入整数（以0结尾）：");
        for (;;){
            n=scan.nextInt();
            if(n==0)break;
            linkint.addNode(n);
        }
        System.out.println("输入完毕！");
        //输出
        linkint.printList();
        //插入节点
        linkint.insertNode(4,a);
        linkint.insertNode(0,b);
        //输出
        linkint.printList();

        //删除节点
        linkint.deleteNode(5);
        //输出
        linkint.printList();
        System.out.println("总数"+linkint.length());
    }
}