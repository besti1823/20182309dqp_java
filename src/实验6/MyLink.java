public class MyLink {
    Node head = null; // 头节点
    int nDongQP = 0;
    public void addNode(int d) {
        Node newNode = new Node(d);// 实例化一个节点
        if (head == null) {
            head = newNode;
            nDongQP++;
            return;
        }
        Node temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = newNode;
        nDongQP++;
    }

    public void insertNode(int index,int d){
        if(index>nDongQP||index<0){
            System.out.println("超出范围！");
            return;
        }

        Node newNode = new Node(d);
        Node temp = head;
        int i;
        for(i=0;i<index-1;i++){
            temp = temp.next;
        }
        newNode.next = temp.next;
        temp.next = newNode;
        nDongQP++;
    }

    public boolean deleteNode(int index) {
        if (index < 1 || index > length()) {
            System.out.println("超出范围！");
            return false;
        }
        if (index == 1) {
            head = head.next;
            nDongQP--;
            return true;
        }
        int i = 1;
        Node preNode = head;
        Node curNode = preNode.next;
        while (curNode != null) {
            if (i == index-1) {
                preNode.next = curNode.next;
                nDongQP--;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            i++;
        }
        return false;
    }

    public int length() {
        return nDongQP;
    }

    public boolean deleteNode11(Node n) {
        if (n == null || n.next == null) {
            return false;
        }
        int tmp = n.data;
        n.data = n.next.data;
        n.next.data = tmp;
        n.next = n.next.next;
        System.out.println("删除成功！");
        return true;
    }

    public void printList() {
        Node tmp = head;
        while (tmp != null) {
            System.out.print(tmp.data+" ");
            tmp = tmp.next;
        }
        System.out.println();
    }

    public void bubbleSort(){
        Node newHead = head;
        for (int i = 0; i < nDongQP; i++) {

            // 前一个元素指针
            Node preNode = null;

            // 当前处理的元素
            Node curNode = newHead;

            for (int j = 0; j < nDongQP - 1 - i; j++) {

                if (curNode.data > curNode.next.data) {

                    // 1、交换两个节点的引用，此时curNode的指针交换后会前移，只需要更新preNode指向即可

                    // 1.1、缓存下下个节点
                    Node tmpNode = curNode.next.next;

                    curNode.next.next = curNode;

                    // 1.2、前驱节点指向nextNode
                    if (preNode != null) {
                        preNode.next = curNode.next;
                        // 1.3、没有前驱节点证明当前节点为头结点，更新头结点
                    }
                    else {
                        newHead = curNode.next;
                    }

                    // 因为需要把preNode指向原来的下个节点，所以此处赋值preNode，preNode后移
                    preNode = curNode.next;

                    // 1.4、curNode指向下下个节点
                    curNode.next = tmpNode;

                    // 更新preNode & curNode指针
                } else {
                    preNode = curNode;
                    curNode = curNode.next;
                }
            }
        }
    }
}