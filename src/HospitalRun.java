public class HospitalRun {
    public static void main(String[] args) {
        Doctor a = new Doctor();
        Nurse b = new Nurse();
        Gov c = new Gov();
        Surgeon d = new Surgeon();
        Receptionist e = new Receptionist();
        Doorman f = new Doorman();

        System.out.println(a.tostring());
        System.out.println(a.Ask());
        System.out.println(a.Do());

        System.out.println(b.tostring());
        System.out.println(b.Ask());
        System.out.println(b.Do());

        System.out.println(c.tostring());
        System.out.println(c.Ask());
        System.out.println(c.Do());

        System.out.println(d.tostring());
        System.out.println(d.Ask());
        System.out.println(d.Do());

        System.out.println(e.tostring());
        System.out.println(e.Ask());
        System.out.println(e.Do());

        System.out.println(f.tostring());
        System.out.println(f.Ask());
        System.out.println(f.Do());
    }
}
