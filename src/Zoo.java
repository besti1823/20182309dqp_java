import javax.swing.*;

public class Zoo {
    private Animal[] animallist;

    public Zoo() {
        animallist = new Animal[5];
        animallist[0] = new Cat("mao","01");
        animallist[1] = new Dog("gou","02");
        animallist[2] = new Pig("zhu","03");
    }

    public void feedAll(){
        animallist[0].feed();
        animallist[1].feed();
        animallist[2].feed();
    }

}
