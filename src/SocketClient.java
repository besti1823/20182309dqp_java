
import java.io.*;
import java.net.Socket;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws IOException {
        /* 1.?????????Socket??????????????&lambda;?&uacute;??? */
        Socket socket = new Socket("192.168.43.118",8800);
//        Socket socket = new Socket("172.16.43.187",8800);

        //2.???socket??��??
        OutputStream outputStream = socket.getOutputStream();
 //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //??????
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.??????????????????????socket???��?��????
        String info1 = " User:Tom Pasword:123456";
//        String info = new String(info1.getBytes("GBK"),"utf-8");
   //     printWriter.write(info);
   //     printWriter.flush();
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //??????????????
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("?????????????????" + reply);
        }
        //4.??????
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
