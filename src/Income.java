public class Income extends PeopleInfo{
    private double income;

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public String toString(){
        return "Income";
    }
}
