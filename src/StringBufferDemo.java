
//***********************************************
//
//     Filename: StringBufferDemo.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-25 09:00:51
//Last Modified: 2019-09-25 09:00:51
//***********************************************

public class StringBufferDemo
{
        public static void main(String[] args)
        {
                StringBuffer buffer = new StringBuffer();
		buffer.append("StringBufferStringBuffer");
		System.out.println(buffer.capacity());
		System.out.println("buffer = " + buffer.toString());
		System.out.println(buffer.length());
        }
}
