
//***********************************************
//
//     Filename: chengxu3_6.java
//
//       Author: dongqipeng - 694986107@qq.com
//  Description: ---
//       Create: 2019-09-18 13:45:40
//Last Modified: 2019-09-18 13:45:40
//***********************************************

public class chengxu3_6
{
	enum Flavor {vanilla, chocolate, strawberry, fudgeRipple, coffee,rockyRoad, mintChocolateChip, cookieDough}
        public static void main(String[] args)
        {
                Flavor cone1, cone2, cone3;
		cone1 = Flavor.rockyRoad;
		cone2 = Flavor.chocolate;
		
		System.out.println ("conel value: "+ cone1);
		System.out.println ("conel ordinal: "+ cone1.ordinal());
		System.out.println ("cone1 name: "+ cone1.name());
		
		System.out.println ();
		System.out.println ( "cone2 value: "+ cone2) ;
		System.out.println ( "cone2 ordinal: "+ cone2.ordinal()) ;
		System.out.println ( "cone2 name: "+ cone2.name());
		
		cone3 = cone1 ;
		
		System.out.println () ;
		System.out.println ("cone3 value: "+ cone3);
		System.out.println ("cone3 ordinal: "+ cone3.ordinal());
		System.out.println ("cone3 name : "+ cone3.name());
        }
}
