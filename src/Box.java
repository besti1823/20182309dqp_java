public class Box {
    private double h;
    private double w;
    private double d;
    private boolean full;

    public Box(double h, double w, double d) {
        this.h = h;
        this.w = w;
        this.d = d;
        full = false;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }

    @Override
    public String toString() {
        return "Box{" +
                "h=" + h +
                ", w=" + w +
                ", d=" + d +
                ", full=" + full +
                '}';
    }
}
