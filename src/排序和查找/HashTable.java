class HashTable{
    private DataItem[] hashArray;
    private int arraySize;
    private DataItem nonItem;
    public HashTable(int size){
        arraySize = size;
        hashArray = new DataItem[arraySize];
        nonItem = new DataItem(-1);
    }
    public void displayTable(){
        System.out.print("Table:");
        for(int i=0;i<arraySize;i++){
            if(hashArray[i] != null){
                System.out.print(hashArray[i].getKey() + " ");
            }else {
                System.out.print("** ");
            }
        }
        System.out.println();
    }
    //哈希化函数
    public int hashFunc(int key){
        return key%arraySize;
    }
    public void insert(DataItem item){
        int key = item.getKey();
        int hashVal = hashFunc(key);//把得到的值哈希化
        while (hashArray[hashVal] != null && hashArray[hashVal].getKey() != -1){    //位置被占用向下找
            hashVal ++;
            hashVal = hashVal%arraySize;    //是否是最后一个位置，是的话往下走
        }
        hashArray[hashVal] = item;
    }

    public DataItem delete(int key) {
        int hashVal = hashFunc(key);
        while (hashArray[hashVal] != null){
            if(hashArray[hashVal].getKey() == key){ //找到要删除的数据
                DataItem temp = hashArray[hashVal];
                hashArray[hashVal] = nonItem;
                return temp;
            }
            hashVal ++;
            hashVal = hashVal%arraySize;//超出数组最大范围
        }
        return null;//没有找到
    }

    public DataItem find(int key) {
        int hashVal = hashFunc(key);
        while(hashArray[hashVal] != null){
            if(hashArray[hashVal].getKey() == key){
                return hashArray[hashVal];
            }
            hashVal ++;
            hashVal = hashVal%arraySize;
        }
        return null;
    }
}